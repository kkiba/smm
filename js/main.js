
var currentTab = 0;
showTab(currentTab);
function showTab(n) {

  var x = document.getElementsByClassName("tab");
  x[n].style.display = "block";

  if (n == 0) {
    document.getElementById("wrapper2").style.display = "none";
  } else {
    document.getElementById("wrapper2").style.display = "inline";
  }
  if (n == (x.length - 1)) {
    document.getElementById("wrapper3").style.display = "block";
  } else {
    document.getElementById("nextBtn").innerHTML = "Далее";
  }

  fixStepIndicator(n)
}

function nextPrev(n) {
  var x = document.getElementsByClassName("tab");
  if (n == 1 && !validateForm()) return false;
  x[currentTab].style.display = "none";
  currentTab = currentTab + n;
  if (currentTab >= x.length) {
    document.getElementById("regForm").submit();
    return false;
  }
  showTab(currentTab);
}

function validateForm() {
  var x, y, i, valid = true;
  x = document.getElementsByClassName("tab");
  y = x[currentTab].getElementsByTagName("input");
  for (i = 0; i < y.length; i++) {
    if (y[i].value == "") {
      y[i].className += " invalid";
      valid = false;
    }
  }
  if (valid) {
    document.getElementsByClassName("step")[currentTab].className += " finish";
  }
  return valid;
}

function fixStepIndicator(n) {
  var i, x = document.getElementsByClassName("step");
  for (i = 0; i < x.length; i++) {
    x[i].className = x[i].className.replace(" active", "");
  }
  x[n].className += " active";
}


$('.slider-sinlge').slick({
       slidesToShow: 1,
       slidesToScroll: 1,
       arrows: true,
       fade: false,
       adaptiveHeight: true,
       infinite: true,
       useTransform: true,
       speed: 400,
       cssEase: 'cubic-bezier(0.77, 0, 0.18, 1)',
   customPaging: function (slider, i) {

       console.log(slider);
       return  (i + 1) + '/' + slider.slideCount;
   }
   });
        var ctx = document.getElementById('myChart').getContext('2d');
   var myChart = new Chart(ctx, {
       type: 'pie',
       data: {
           datasets: [{
               data: [45, 55],
               backgroundColor: [
                 'rgb(238, 155, 252)',
                 'rgb(126, 127, 217)',
               ],
               borderColor: [
                   'rgb(238, 155, 252)',
                   'rgb(126, 127, 217)',
               ],
               borderWidth: 1
           }]
       },
   });
   var ctx = document.getElementById('chartage').getContext('2d');
   var chartage = new Chart(ctx, {
       type: 'pie',
       data: {
           datasets: [{
               data: [59, 31],
               backgroundColor:['#fff','#fff' ],
               borderColor: [
                   'rgb(243, 224, 148)',
                   'rgb(162, 166, 166)',
               ],
               borderWidth: 3
          }]
       },
   });
$('.slider-owl').owlCarousel(function(){

});
